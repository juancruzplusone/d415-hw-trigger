from camTrigger import CamTrigger


def main():
    # Create a CamTrigger object
    cam_trigger = CamTrigger(port='/dev/ttyACM0', baud=9600,
                             startDelay=500, betweenPulseDelay=500)

    # Update the delay of start trigger
    cam_trigger.updateDelayOfStartTrigger(100)

    # Update the delay between pulses
    cam_trigger.updateDelayBetweenPulses(100)

    # Send the trigger
    cam_trigger.sendTrigger()

    cam_trigger.closeConnection()


if __name__ == '__main__':
    main()
