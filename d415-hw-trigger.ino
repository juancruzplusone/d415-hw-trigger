// Declare variables
// Declare output pins
const int outputPins[] = {2, 3, 4};

// delays in milliseconds
int triggerStartDelay = 0;
int delayBetweenTriggers = 0;

void generatePulse() 
{
  delay(triggerStartDelay);
  for(int i = 0; i < sizeof(outputPins)/sizeof(int); i++)
  {
    digitalWrite(outputPins[i], HIGH);
    delayMicroseconds(100); // As specified here: https://dev.intelrealsense.com/docs/multiple-depth-cameras-configuration#e-external-trigger
    digitalWrite(outputPins[i], LOW);
    delay(delayBetweenTriggers);
  }
}

void setup() 
{
  Serial.begin(9600);

  // Configure output pins
  for(int i = 0; i < sizeof(outputPins)/sizeof(int); i++)
  {
    pinMode(outputPins[i], OUTPUT);
    digitalWrite(outputPins[i], LOW);
  }
}


void loop()
{
  // Check if data is available to read
  if (Serial.available()) 
  {
    // Read the incoming data
    String data = Serial.readStringUntil('\n');

    // Check if the data is for trigger start delay or delay between triggers
    if (data.startsWith("TSD:")) 
    {
      // Update trigger start delay
      triggerStartDelay = data.substring(4).toInt();

    }
    else if (data.startsWith("DBT:")) 
    {
      // Update delay between triggers
      delayBetweenTriggers = data.substring(4).toInt();
    }
    else if (data == "Start")
    {
      generatePulse();
    }
  }
}