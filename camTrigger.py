import serial
import queue
import time

class CamTrigger:
    """Class to manage the triggering of RealSense D415 cameras via an Arduino board.

    This class interfaces with an Arduino via a serial connection to control the
    triggering of multiple RealSense D415 cameras. The Arduino sends a trigger
    pulse to each camera at specified intervals.
    """

    def __init__(self, port, baud, startDelay, betweenPulseDelay):
        """Initializes the CamTrigger instance.

        Args:
            port (str): The port that the Arduino is connected to.
            baud (int): The baud rate for the serial connection.
            startDelay (int): The delay in milliseconds before the first trigger pulse is sent after the start command.
            betweenPulseDelay (int): The delay in milliseconds between trigger pulses for each camera.
        """
        self.ser = serial.Serial(port=port, baudrate=baud)
        self.input_queue = queue.Queue()
        self.startDelay = startDelay
        self.betweenPulseDelay = betweenPulseDelay

    def updateDelayOfStartTrigger(self, startDelay):
        """Updates the delay before the first trigger pulse is sent.

        Args:
            startDelay (int): The new delay in milliseconds.
        """
        self.input_queue.put("TSD:" + str(startDelay))
        self.__sendInstructions()

    def updateDelayBetweenPulses(self, betweenPulseDelay):
        """Updates the delay between trigger pulses for each camera.

        Args:
            betweenPulseDelay (int): The new delay in milliseconds.
        """
        self.input_queue.put("DBT:" + str(betweenPulseDelay))
        self.__sendInstructions()

    def sendTrigger(self):
        """Sends the start command to begin triggering the cameras."""
        self.input_queue.put("Start")
        self.__sendInstructions()

    def closeConnection(self):
        """Closes the serial connection to the Arduino."""
        self.ser.close()

    def __sendInstructions(self):
        """Sends a command from the input queue to the Arduino over the serial connection.

        This method should not be called directly. It is automatically called after
        one of the public methods puts a command in the queue.
        """
        txMsg = self.input_queue.get()
        self.ser.write((txMsg + '\n').encode('utf-8'))
        time.sleep(0.1)
