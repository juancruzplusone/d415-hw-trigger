# D415 External Hardware Trigger via Arduino Micro

This package provides the microcontroller source code and Python control module for using an Arduino Micro board as an external hardware trigger for 3 Realsense D415 Cameras.

Interactions with the controller are now handled programmatically through the `CamTrigger` class.

Upon triggering, the microcontroller sends a sync pulse to the cameras, instructing them to align their frame timestamps.

Each camera's frame counter will now be synchronized, but their timestamps will be offset depending on the user-inputted "Delay Between Triggers". This delay represents the time between the start of each camera's capture, enabling staggered captures across multiple cameras.

# Usage
1. Connect Arduino Micro board to computer.

2. Import the `CamTrigger` class from the `cam_trigger.py` file.

```python
from cam_trigger import CamTrigger
```
3. Instantiate a CamTrigger object with the appropriate port, baud rate, start delay, and delay between triggers. **Time values are in milliseconds.**

```python
cam_trigger = CamTrigger('/dev/ttyACM0', 9600, 1000, 500)
```

4. Update the delay parameters as needed with the updateDelayOfStartTrigger and updateDelayBetweenPulses methods.

```python
cam_trigger.updateDelayOfStartTrigger(1500)
cam_trigger.updateDelayBetweenPulses(750)
```

5. Initiate the trigger sequence with the sendTrigger method.

```python
cam_trigger.sendTrigger()
```
6. Once you are done, close the serial connection with the closeConnection method.
```python
cam_trigger.closeConnection()
```

Please ensure that the Arduino Micro board is connected properly and the port name is correct when establishing the serial connection.

For a detailed example of how to use the CamTrigger class, refer to the [example_camTrigger.py](example_camTrigger.py) script provided in this repository.
